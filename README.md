# README #

### giftbox ###

Projet Giftbox de :
Romain Marlier -- marlier11u
Axel Froehlicher -- froehlic7u
Pierre Berger -- berger35u
Samy Elayachi -- elayachi7u

[Lien de test](https://sociallaunch.fr/giftbox/index.php)

#Tester le site localement#

Il faut d'abord télécharger le dépot, ensuite il faut ajouter dans une base de donnée nommée "giftbox" les fichiers .sql afin de charger les données.

Une fois que la base de donnée est installé, vous devez ajoutez un fichier conf.ini dans le dossier /src/conf/
sous la forme suivante :

- driver=mysql
- username=               <- votre pseudo pour acceder a votre base
- password=               <- votre mot de pass
- database=               <- le nom de votre base
- host=                   <- adresse de la base
- charset=utf8
- collation=utf8_unicode_ci

Pour terminer, il faut effectuer un composer install.