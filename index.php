<?php
require_once 'vendor/autoload.php';
session_start();

use \conf\Eloquent;
use \giftbox\controllers\ControlCatalogue;
use \giftbox\controllers\ControlBox;
use \giftbox\controllers\ControlGestionnaire;
use \Slim\Slim;

Eloquent::init('src/conf/conf.ini');

$app = new Slim();

$app->get('/', function(){
    (new ControlCatalogue())->acceuil();
});

$app->get('/prestations/listPrest', function(){
    (new ControlCatalogue())->listerPrestations();
});

$app->get('/prestations/view/:id', function($id){
    (new ControlCatalogue())->viewPrestation($id);
});

$app->get('/prestations/listCat', function(){
    (new ControlCatalogue())->listeCategorie();
});

$app->get('/prestations/cat/:id', function($idCategorie){
    (new ControlCatalogue())->viewPrestationCat($idCategorie);
});

$app->post('/box/add/:id/', function($idPrestation){
    (new ControlBox())->ajouterPrestation($idPrestation);
    $app = Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/prestations/listPrest');
});

$app->get('/prestations/viewPriceAsc', function(){
    (new ControlCatalogue())->viewPrestationPrixAsc();
});

$app->get('/prestations/viewPriceDesc', function(){
    (new ControlCatalogue())->viewPrestationPrixDesc();
});

$app->get('/box/view', function(){
    (new ControlBox())->afficherBox();
});

$app->post('/box/delete/:id', function ($id){
    (new ControlBox())->supprimerPanier($id);
});

$app->post('/box/addPrest/:token/:id',function ($token,$id){
    (new ControlBox())->supprimerArticle($token,$id);
});

$app->get('/box/validateInfo', function (){
    (new ControlBox())->validerPanier();
});

$app->post('/box/validateRecap', function (){
    (new ControlBox())->validateRecap();
});

$app->post('/box/validate', function (){
    (new ControlBox())->validate();
});

$app->get('/box/recap', function (){
    (new ControlBox())->recap();
});

$app->post('/box/delete/:box/:id', function ($box,$id){
    (new ControlBox())->delete($box,$id);
    $app = Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/box/edit');
});

$app->get('/box/view/:token', function($token){
    (new ControlBox())->afficherBoxToken($token);
});

$app->post('/box/empty', function (){
    (new ControlBox())->viderPanier();
});

$app->post('/box/generate/:id', function ($id){
    (new ControlBox())->genererCadeau($id);
    $app = \Slim\Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/box/view/'.$id);
});

$app->get('/box/edit/:box', function($box){
    (new ControlBox())->editBox($box);
});

$app->get('/box/pay/:box', function ($box){
    (new ControlBox())->pay($box);
});

$app->post('/box/view/:token', function($token){
    (new ControlBox())->afficherBoxToken($token);
});

$app->get('/prestations/rate/:id/:note', function ($id,$note){
    (new ControlCatalogue())->rate($id,$note);
    $app = Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/prestations/rateInfo');
});

$app->get('/prestations/rateInfo', function (){
    (new ControlCatalogue())->validateRate();
});

$app->get('/box/login/:box', function ($box){
    (new ControlBox())->login($box);
});

$app->post('/box/auth/:box', function ($box){
    (new ControlBox())->auth($box);
});

$app->get('/gift/:id', function ($id){
    (new ControlBox())->giftView($id);
});

$app->get('/cagnotte/:id', function ($id){
    (new ControlBox())->cagnotteView($id);
});

$app->post('/cagnotte/pay/:id', function ($id){
    (new ControlBox())->cagnottePay($id);
});
$app->get('/cagnotte/edit/:id', function ($id){
    (new ControlBox())->cagnotteViewEdit($id);
});

$app->post('/cagnotte/edit/pay/:id', function ($id){
    (new ControlBox())->cagnottePay($id,true);
});

$app->post('/cagnotte/cloturer/:id', function ($id){
    (new ControlBox())->cloturerCagnotte($id);
});

$app->get('/gest/login', function (){
    (new ControlGestionnaire())->login();
});

$app->post('/gest/login/', function(){
    (new ControlGestionnaire())->loginPost();
});

$app->get('/gest/auth/', function(){
    (new ControlGestionnaire())->accueil();
});

$app->post('/gest/delete/:id', function($id){
    (new ControlGestionnaire())->delete($id);
    $app = Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/gest/auth?modif=1');
});

$app->post('/gest/disable/:id', function($id){
    (new ControlGestionnaire())->disable($id);
    $app = Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/gest/auth?modif=2');
});

$app->post('/gest/active/:id', function($id){
    (new ControlGestionnaire())->active($id);
    $app = Slim::getInstance();
    $app->redirect($app->request->getRootUri().'/gest/auth?modif=3');
});

$app->get('/gest/addPrest', function(){
    (new ControlGestionnaire())->addPrest();
});

$app->post('/gest/addPrest', function(){
    (new ControlGestionnaire())->addPrestPost();
});

$app->run();