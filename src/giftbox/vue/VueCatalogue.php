<?php
namespace giftbox\vue;


class VueCatalogue
{
    private $collection;

    public function __construct($p)
    {
        $this->collection = $p;
    }

    private function htmlListPresta(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();


        $r='<div class="container">';
        if(isset($_GET['error'])) {
            if ($_GET['error'] == 1) {
                $r .= '<div class="alert alert-danger" role="alert">Cette prestation n\'existe pas.</div>';
            }
        }
        $nbParcour = 0;
        $taille= count($this->collection);
        foreach ($this->collection as $q){
            if($q->etat=='actif') {
                if ($nbParcour % 4 == 0) {
                    $r .= '<div class="row">';
                }
                $r .= '<div class="col-md-3" style="">'
                    . '<a href="' . $route . '/prestations/view/' . $q->id . '" class="thumbnail" style="height: 100%">'

                    . '<img src="' . $route . '/../web/img/' . $q->img . '" alt="' . $q->nom . '" style="height:20em">'
                    . '<div class="caption">'
                    . '<h3>' . $q->nom . '</h3>'

                    . '<p>' . $q->prix . '€</p>'
                    . '<p>' . $q->categorie['nom'] . ' </p>'
                    . '</div></a>';

                if ($nbParcour % 4 == 3 || $nbParcour == $taille - 1) {

                    $r .= '</div>';
                }
                $r .= '</div>';
                $nbParcour++;
            }
        }
        $r.='</div>';
        return $r;
    }

    private function htmlViewPresta(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r='<div class="container">'
            .'<div class="thumbnail">';

        if(!is_null($this->collection)){
            $r.='<img src="'.$route.'/../web/img/'.$this->collection->img.'" alt="'.$this->collection->nom.'">'

                .'<div class="caption">'
                .'<h3 class="text-center">'.$this->collection->nom.'</h3>'
                .'<p class="text-center">'.$this->collection->descr.'</p>'
                .'<p class="text-center">'.$this->collection->prix.'€</p>';
            $moy = round($this->collection->note()->avg('note'),1);
            if($moy == 0)
                $moy = 'Pas de note pour le moment !';
            $r.='<p class="text-center"><strong>Note moyenne : </strong>'.$moy.'</p>';

            $r.='<p>
                    <form class="text-center" method="post" action="' . $route .'/box/add/'.$this->collection->id.'">'
                .'<button class="btn btn-primary" role="button">Ajouter au panier</button></form></p>'
                .'<div class="rating"><!--
                       --><a href="'.$route.'/prestations/rate/'.$this->collection->id.'/5" title="Donner 5 étoiles">☆</a><!--
                       --><a href="'.$route.'/prestations/rate/'.$this->collection->id.'/4" title="Donner 4 étoiles">☆</a><!--
                       --><a href="'.$route.'/prestations/rate/'.$this->collection->id.'/3" title="Donner 3 étoiles">☆</a><!--
                       --><a href="'.$route.'/prestations/rate/'.$this->collection->id.'/2" title="Donner 2 étoiles">☆</a><!--
                       --><a href="'.$route.'/prestations/rate/'.$this->collection->id.'/1" title="Donner 1 étoile">☆</a>
                    </div>'
                .'</div></div></div></div>';
        }
        $r.='</div></div>';
        return $r;
    }

    private function htmlListCat(){

        $r='<div>';
        if(isset($_GET['error'])) {
            if ($_GET['error'] == 1) {
                $r .= '<div class="alert alert-danger" role="alert">Cette catégorie n\'existe pas.</div>';
            }
        }
        $r.='<ul class="list-group">';
        $r.='<p style="font-size: 20pt "><B>&nbsp Liste des catégories :</B></p>';
        foreach ($this->collection as $c){
            $nb=$c->prestations()->where('etat','=','actif')->count();
            $r.='<a href="cat/'.$c->id.'" class="list-group-item" style="height:50px; width: 200px;margin-left: 5%"><span class="badge">'.$nb.'</span>'.$c->id . '. ' . $c->nom . '</a>';
        }
        $r .= '</ul></div>';
        return $r;
    }

    private function validate(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r='<div class="alert alert-info">';
        $r.='<strong>Info!</strong> Nous vous remercions pour votre note.';
        $r.='</div>';
        $r.='<form class="text-center" method="get" action="javascript:history.go(-1)">'
            .'<button class="btn btn-primary" type="submit">Retour</button>'
            .'</form>';
        return $r;
    }

    private function htmlViewAccueil(){
        $top = $this->collection['top'];
        $topCat = $this->collection['topCat'];
        $route = \Slim\Slim::getInstance()->request()->getRootUri();

        $r = '<h1 class="text-center">Bienvenue sur GiftBox !</h1>';
        $r.='<p class="text-center">Vous voilà sur GiftBox, vous allez pouvoir créer une box pour l\'un de vos amis.
            Une large selection de prestations vous est proposée.</p><br>';

        $r .='<h2 class="text-center"> La meilleure prestation du moment :</h2>';
        $r.='<div class="container">'
            .'<a class="thumbnail" href="'.$route.'/prestations/view/'.$top->id.'">';

        if(!is_null($this->collection)) {
            $r .= '<img src="' . $route . '/../web/img/' . $top->img . '" alt="' . $top->nom . '">'

                . '<div class="caption">'
                . '<h3 class="text-center">' . $top->nom . '</h3>'
                . '<p class="text-center">' . $top->descr . '</p>'
                . '<p class="text-center">' . $top->prix . '€</p>';
            $moy = round($top->note()->avg('note'), 1);
            if ($moy == 0)
                $moy = 'Pas de note pour le moment !';
            $r .= '<p class="text-center"><strong>Note moyenne : </strong>' . $moy . '</p>';

            $r .='</div></div></a>';
        }
        $r.='<h2 class="text-center">Les meilleures prestations par catégorie :</h2><br>';




        $r.='<div class="container text-center">';
        $nbParcour = 0;
        $taille= count($topCat);
        $r.='<div class="row">';
        foreach ($topCat as $q){

            $r.='<div class="col-md-3" style="">'
                .'<a href="'.$route.'/prestations/view/'.$q->id.'" class="thumbnail" style="height: 100%">'

                .'<img src="'.$route.'/../web/img/'.$q->img.'" alt="'.$q->nom.'" style="height:15em">'
                .'<div class="caption">'
                .'<h3>'.$q->nom.'</h3>'

                .'<p>'.$q->prix.'€</p>'
                .'<p>'.$q->categorie['nom'].' </p>';
            $moy = round($q->note()->avg('note'), 1);
            if ($moy == 0)
                $moy = 'Pas de note !';
            $r .= '<p class="text-center"><strong>Note moyenne : </strong>' . $moy . '</p>';
            $r.='</div></div></a>';

        }
        $r.='</div>';

        return $r;

    }

    public function render($i){
        switch($i){
            case '1':
                $content=$this->htmlListPresta();
                break;
            case '2':
                $content=$this->htmlViewPresta();
                break;
            case '3':
                $content=$this->htmlListCat();
                break;
            case '4':
                $content=$this->validate();
                break;
            case '5':
                $content=$this->htmlViewAccueil();
                break;
            default :
                $content='';
        }

        $route = \Slim\Slim::getInstance()->request()->getRootUri();

        include 'header.php';
        echo <<<END

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        
        <link href="$route/../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="$route/../web/css/GiftboxCss.css" rel="stylesheet" type="text/css">
        <title>Giftbox</title>        
    </head>
    
    <body>
        $header
        $content
        
     
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="$route/../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
        
        
    </body>
</html>

END;
    }


}