<?php
namespace giftbox\vue;
$route = \Slim\Slim::getInstance()->request()->getRootUri();

if(isset($_SESSION['nbArticle'])){
    $article = $_SESSION['nbArticle'];
}else{
    $article = 0;
}
$header='
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="'.$route.'">Giftbox</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="'.$route.'">Accueil<span class="sr-only">(current)</span></a></li>
        
        <li class="dropdown">
          <a href="'.$route.'/prestations/listPrest" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Prestations <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="'.$route.'/prestations/listPrest">Liste des prestations</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="'.$route.'/prestations/viewPriceAsc">Liste par prix croissant</a></li>
            <li><a href="'.$route.'/prestations/viewPriceDesc">Liste par prix décroissant</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="'.$route.'/prestations/listCat">Liste des catégories</a></li>
          </ul>
        </li>
        
      </ul>
      
     
      <ul class="nav navbar-nav navbar-right">
        <li> 
            <a href="'.$route.'/gest/login"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Gestion</a>
            </li>
           
        <li> 
            <a href="'.$route.'/box/view"><span class="	glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Panier <span class="badge">'.$article.'</span></a>
            </li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
';

