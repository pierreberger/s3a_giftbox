<?php
/**
 * Created by PhpStorm.
 * User: Axel
 * Date: 13/12/2016
 * Time: 12:28
 */

namespace giftbox\vue;


use giftbox\models\Cagnotte;
use giftbox\models\Prestation;

class VueBox
{
    private $box;

    public function setBox($b){
        $this->box=$b;
    }

    private function htmlViewBox()
    {
        $route = \Slim\Slim::getInstance()->request()->getRootUri();

        $r='';
        if(isset($_GET['error'])){
            if($_GET['error']==1){
                $r.='<div class="alert alert-danger" role="alert">Votre Panier est vide.</div>';
            }elseif($_GET['error']==2){
                $r.='<div class="alert alert-warning" role="alert">Votre panier doit contenir des prestations de deux catégories différentes.</div>';
            }elseif($_GET['error']==3){
                $r.='<div class="alert alert-danger" role="alert">Votre Box n\'existe pas.</div>';
            }
        }




        $r.='<div class="container">'
            .'<h1>Panier</h1>';
        if(isset($_SESSION['box'])) {
            foreach ($_SESSION['box'] as $q) {

                $r.='<div class="media"><div class="media-left" >'
                    . '<a href="#">'
                    . '<img class="media-object" src="'.$route.'/../web/img/'.$q->img.'" alt="'.$q->nom.'" style="width: 128px; height: 128px;">'
                    . '</a>'
                    . '</div>'
                    . '<div class="media-body">'
                    . '<h4 class="media-heading">'.$q->nom.'</h4>'
                    .'<p>'.$q->categorie->nom.'<br>'.'</p>'
                    .'<p>'.$q->prix.'€ '.'</p>'
                    .'<form style="display:inline"  method="POST" action="' . $route . '/box/delete/'.$q->id.'">
                        <button type="submit" name="delprest" class="btn btn-danger">Supprimer</button>
                    </form>'
                    .'</div></div>';
            }
        }
        $r.='<br><ul class="list-group">';
        $r.='<label for="mon_id">Montant Total</label><li class="list-group-item" style="color: darkblue;">'.$_SESSION['prixTotal'].' €</li>';
        $r.='</ul>';
        $r.='<form  method="POST" action="' . $route . '/box/empty">
                        <button type="submit" class="btn btn-danger">Vider le Panier</button>
                    </form>';
        $r.='<br><a href="'.$route.'/box/validateInfo"><button class="btn btn-primary" role="button">Valider</button></a>';
        $r.='</div>';

        return $r;
    }

    private function htmlValidateBox(){

        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r='<div class="container">';
        if(isset($_GET['error'])){
            if($_GET['error']==1) {
                $r .= '<div class="alert alert-warning" role="alert">Veuillez verifier les données.</div>';
            }
            if($_GET['error']==2){
                $r.='<div class="alert alert-warning" role="alert">Le mot de passe doit être identique.</div>';
            }
            if($_GET['error']==3){
                $r.='<div class="alert alert-warning" role="alert">Le mot de passe doit faire une taille minimum de 4 caractères.</div>';
            }
        }

        $r .= '<form id="form1" method="POST" action="' . $route . '/box/validateRecap">';

        $r .='<label for="pass">Entrez vos informations :</label><br>';

        if (isset($_GET['modif']) && $_GET['modif'] == 1 && isset($_SESSION['nom']) && isset($_SESSION['prenom']) && isset($_SESSION['email']) && isset($_SESSION['message']) && isset($_SESSION['modepay'])) {

            $r .= '<input type="text" value="' . $_SESSION['nom'] . '" name="nom" placeholder="Nom"><br>';
            $r .= '<input type="text"  value="' . $_SESSION['prenom'] . '" name="prenom" placeholder="Prenom"><br>';
            $r .= '<input type="email"  value="' . $_SESSION['email'] . '" name="email" placeholder="Email"><br>';
            $r .= '<input type="text" value="' . $_SESSION['message'] . '" name="message" placeholder="Message"><br>';
            $r .= '<label for="pays">Choisissez un mode de paiement</label><br>';
            $r .= '<select name="modepay" id="modepay">';
            if ($_SESSION['modepay'] == 'classique') {
                $r .= '<option value="classique">Classique</option>';
                $r .= '<option value="cagnotte">Cagnotte</option>';
            } else {
                $r .= '<option value="cagnotte">Cagnotte</option>';
                $r .= '<option value="classique">Classique</option>';
            }

            $r .= '</select><br><br>';

        } else{

            $r .= '<input type="text" name="nom" placeholder="nom"><br>';
            $r .= '<input type="text" name="prenom" placeholder="prenom"><br>';
            $r .= '<input type="email" name="email" placeholder="email"><br>';
            $r .= '<input type="text" name="message" placeholder="message"><br><br>';

            $r .= '<label for="pays">Choisissez un mode de paiement :</label><br>';
            $r .= '<select name="modepay" id="modepay">';
            $r .= '<option value="classique">Classique</option>';
            $r .= '<option value="cagnotte">Cagnotte</option>';

            $r .= '</select><br><br>';

        }

        $r .= '<button class="btn btn-primary" type="submit" name="valider">Valider</button>';
        $r .= '</form>';
        $r.='</div>';

        return $r;
    }

    public function htmlValidateRecap(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r='<div class="container">';
        if(isset($_GET['error'])){
            if($_GET['error']==2){
                $r.='<div class="alert alert-warning" role="alert">Le mot de passe doit être identique !</div>';
            }
            if($_GET['error']==3){
                $r.='<div class="alert alert-warning" role="alert">Le mot de passe doit faire une taille minimum de 4 caractères.</div>';
            }
        }
        $r.='<div class="row"><div class="col-md-6">';
        $r.='<h1>Récapitulatif</h1>';
        $r.='<ul class="list-group">';
        $r.='<label for="mon_id">Prenom</label><li class="list-group-item">'.$_SESSION['prenom'].'</li><br>';
        $r.='<label for="mon_id">Nom</label><li class="list-group-item">'.$_SESSION['nom'].'</li><br>';
        $r.='<label for="mon_id">Email</label><li class="list-group-item">'.$_SESSION['email'].'</li><br>';
        $r.='<label for="mon_id">Message</label><li class="list-group-item">'.$_SESSION['message'].'</li><br>';
        $r.='<label for="mon_id">Montant Total</label><li class="list-group-item">'.$_SESSION['prixTotal'].' €</li><br>';
        $r.='<label for="mon_id">Mode de paiement</label><li class="list-group-item">'.$_SESSION['modepay'].'</li><br>';
        $r.='<label for="mon_id">Mot de passe</label>';
        $r .= '<form method="POST" action="' . $route . '/box/validate">';
        $r .= '<input type="password" name="pass" placeholder="mot de passe (optionnel)"><br>';
        $r .= '<input type="password" name="passConfirm" placeholder="confirmez le mot de passe"><br>';
        $r.='</ul></div><div class="col-md-6"><h1>Prestations</h1>';
        if(isset($_SESSION['box'])) {
            foreach ($_SESSION['box'] as $q) {

                $r.='<div class="media"><div class="media-left" >'
                    . '<a href="#">'
                    . '<img class="media-object" src="'.$route.'/../web/img/'.$q->img.'" alt="'.$q->nom.'" style="width: 128px; height: 128px;">'
                    . '</a>'
                    . '</div>'
                    . '<div class="media-body">'
                    . '<h4 class="media-heading">'.$q->nom.'</h4>'
                    .'<p>'.$q->categorie->nom.'<br>'.'</p>'
                    .'<p>'.$q->prix.'€ '.'</p>'

                    .'</div></div>';
            }
        }


        $r.='</div></div><br>';


        $r .= '<button class="btn btn-primary" type="submit" name="valider">Valider</button>';
        $r .= '</form>';

        $r.='<a href="'.$route.'/box/validateInfo?modif=1"><button class="btn btn-primary"  role="button">Modifier les informations</button></a>';


        return $r;
    }

    public function htmlValidateDone(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $host = \Slim\Slim::getInstance()->request()->getHost();
        $r='<div class="container"><div class=" col-sm-6 col-sm-push-3">';
        $r.='<div class="panel panel-success"> 
          <div class="panel-heading">Voici l\'URL de votre coffret. NE LE PERDEZ PAS !</div>
          <div class="panel-body">';
        $r.= '<a href="'.$route.'/box/view/'.$_SESSION['token'].'">http://'.$host.$route.'/box/view/'.$_SESSION['token'].'</a>';
        $r.='</div>
        </div><div class="text-center">';
        $r.='</div> </div></div>';
        return $r;
    }

    public function htmlViewBoxToken(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $host = \Slim\Slim::getInstance()->request()->getHost();
        $r ='<div class="container">';
        $r.='<div class="row"><div class="col-md-6">';
        $r.='<h1>Visualisation du coffret</h1>';
        if(isset($this->box)) {
            foreach ($this->box->contient()->get() as $q) {
                $prest = Prestation::where('id',$q->idPrest)->first();

                $r.='<div class="media"><div class="media-left" >'
                    . '<a href="#">'
                    . '<img class="media-object" src="'.$route.'/../web/img/'.$prest->img.'" alt="'.$prest->nom.'" style="width: 128px; height: 128px;">'
                    . '</a>'
                    . '</div>'
                    . '<div class="media-body">'
                    . '<h4 class="media-heading">'.$prest->nom.'</h4>'
                    .'<p>'.$prest->categorie->nom.'<br>'.'</p>'
                    .'<p>'.$prest->prix.'€ '.'</p>'

                    .'</div></div>';
            }
            $r.='</div><div class="col-md-6">';
            $r.='<ul class="list-group">';
            $r.='<label for="mon_id">Prénom</label><li class="list-group-item">'.$this->box->prenom.'</li>';
            $r.='<label for="mon_id">Nom</label><li class="list-group-item">'.$this->box->nom.'</li>';
            $r.='<label for="mon_id">Email</label><li class="list-group-item">'.$this->box->mail.'</li>';
            $r.='<label for="mon_id">Message</label><li class="list-group-item">'.$this->box->message.'</li>';
            $r.='<label for="mon_id">Etat</label><li class="list-group-item">'.$this->box->etat.'</li>';
            $r.='<label for="mon_id">Montant total</label><li class="list-group-item">'.$this->box->prixTotal.' €</li>';
            $r.='<label for="mon_id">Mode de paiement</label><li class="list-group-item">'.$this->box->modePay.'</li>
            </ul>';
        }
        $r.='</div></div>';

        if($this->box->etat == 'valider'){
            $r.='<br><a href="'.$route.'/box/edit/'.$this->box->token.'"><button class="btn btn-primary" role="button">Modifier le coffret</button></a>';

            if($this->box->modePay== 'classique') {
                $r .= '<a href="' . $route . '/box/pay/' . $this->box->token . '"><button class="btn btn-primary" role="button">Payer le coffret</button></a>';
                $r.='</div>';
            }else{
                $r.='</div><div class="container text-center">';
                $cagnotte=Cagnotte::where('idbox', '=', $this->box->id)->first();

                if(!is_null($cagnotte->tokenPartage)) {
                    $r .= '<br><div class="col-md-6">';
                    $r .= '<div class="panel panel-info">';

                    $r .= '<div class="panel-heading">Url pour le partage de la cagnotte</div>';
                    $r .= '<div class="panel-body text-center">';
                    $r .= '<a href="' . $route . '/cagnotte/' . $cagnotte->tokenPartage . '">http://' . $host . $route . '/cagnotte/' . $cagnotte->tokenPartage . '</a>';
                    $r .= '</div>';
                    $r .= '</div></div>';
                }

                if(!is_null($cagnotte->tokenEdition)) {
                    $r .= '<div class="col-md-6">';
                    $r .= '<div class="panel panel-info">';

                    $r .= '<div class="panel-heading">Url pour la gestion de la cagnotte</div>';
                    $r .= '<div class="panel-body text-center">';
                    $r .= '<a href="' . $route . '/cagnotte/edit/' . $cagnotte->tokenEdition . '">http://' . $host . $route . '/cagnotte/edit/' . $cagnotte->tokenEdition . '</a>';
                    $r .= '</div>';
                    $r .= '</div></div>';
                }
                $r .= '</div>';
            }

        }else{

            $r.='</div><div class="container">';
            if(!is_null($this->box->urlCadeau)){
                $r.='<br><div class=" col-sm-6 col-sm-push-3 text-center">';
                $r.='<div class="panel panel-info">';
                $r.='<div class="panel-heading">Url Cadeau. A envoyer au destinataire</div>';
                $r.='<div class="panel-body text-center">';
                $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'">http://'.$host.$route.'/gift/'.$this->box->urlCadeau.'</a>';
                $r.='</div>';
                $r.='</div></div>';
            }else{

                $r.='<br><br><form  method="POST" action="' . $route . '/box/generate/'.$this->box->token.'">
                        <button type="submit" class="btn btn-danger">Générer l\'URL cadeau</button>
                    </form>';
            }

            $r.='</div>';
        }

        return $r;
    }

    public function htmlViewBoxEdit(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r ='<div class="container">';
        $r .='<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Ajouter une Prestation</button><br><br>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ajouter une prestattion</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">';
        $r .= '<table class="table table-hover" style="background-color: white">
        <thead>
              <tr>
                <th>Nom</th>
                <th>Prix</th>
                <th>Catégorie</th>
                <th>Ajouter</th>
              </tr>
            </thead>
            <tbody>';

        foreach (Prestation::all() as $p){
            $r .='<tr>';
            $r .='<td>'.$p->nom.'</td>';
            $r .='<td>'.$p->prix.'</td>';
            $r .='<td>'.$p->categorie->nom.'</td>';
            $r .='<td><form style="display:inline" method="POST" action="' . $route . '/box/addPrest/'.$this->box->token.'/'.$p->id.'">
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </form></td>';
            $r .='</tr>';
        }

        $r .= '            </tbody></table></div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                            
                          </div>
                        </div>
                      </div>
                    </div>';
        if(isset($_GET['error']))
            $r.='<div class="alert alert-info"><strong>Info!</strong> Vous devez avoir au moins 2 catégories de prestations.</div>';

        $r.='<h1>Visualisation du coffret</h1>';
        if(isset($this->box)) {
            foreach ($this->box->contient()->get() as $q) {

                $prest = Prestation::where('id',$q->idPrest)->first();

                $r.='<div class="media"><div class="media-left" >'
                    . '<a href="#">'
                    . '<img class="media-object" src="'.$route.'/../web/img/'.$prest->img.'" alt="'.$prest->nom.'" style="width: 128px; height: 128px;">'
                    . '</a>'
                    . '</div>'
                    . '<div class="media-body">'
                    . '<h4 class="media-heading">'.$prest->nom.'</h4>'
                    .'<p>'.$prest->categorie->nom.'<br>'.'</p>'
                    .'<p>'.$prest->prix.'€ '.'</p>'
                    .'
                    <form style="display:inline" id="formDelPrest" method="POST" action="' . $route . '/box/delete/'.$this->box->token.'/'.$prest->id.'">
                        <button type="submit" name="delprest" class="btn btn-danger">Supprimer</button>
                    </form>
           

                    </div></div>';
            }

        }
        $r.='<br><a href="'.$route.'/box/view/'.$this->box->token.'" > <button type="submit" class="btn btn-primary">Valider</button></a>';
        $r.='</div>';
        return $r;

    }

    public function htmlViewBoxPay(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r ='<div class="alert alert-info"><strong>Merci d\'avoir payé votre Box.</strong></div>';
        $r .='<form style="display:inline" method="GET" action="' . $route . '/box/view/'.$this->box->token.'">
                        <button type="submit" class="btn btn-primary">Retour à la box</button>
                    </form>';
        return $r;
    }

    public function htmlViewLogIn(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $host = \Slim\Slim::getInstance()->request()->getHost();
        $r='<div class="container"><div class=" col-sm-6 col-sm-push-3 text-center">';
        $r.='<div class="panel panel-success"><div class="panel-heading">Votre Box requiert un mot de passe !</div>';

        if(isset($_GET['error']) && $_SESSION['error']=1){
            $r .='<div class="alert alert-danger"><strong>Mot de passe incorrect !</strong></div>';
        }

        $r .='<div class="panel-body text-center">';

        $r .= '<form id="form1" method="POST" action="' . $route . '/box/auth/'.$this->box->token.'">';


        $r .= '<label for="pays">Entrez votre mot de passe :</label><br>';
        $r .= '<input type="password" name="pass" placeholder="mot de passe"><br>';
        $r .= '<br><button type="submit" class="btn btn-primary">Valider</button>';

        $r .= '</form>';

        $r.='</div>
        </div><div class="text-center">';
        $r.='</div> </div></div>';
        return $r;
    }
    public function htmlViewGift(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $host = \Slim\Slim::getInstance()->request()->getHost();
        $r ='<div class="container">';
        $r.='<h1 class="text-center">'.$this->box->message.'</h1>';
        $r .='<hr>';
        if(!isset($_GET['aff'])){
            $r .= '<div class="text-center"><label  for="pays">Choisissez un affichage :</label><br>';
            $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'?aff=global"><button class="btn btn-success"  role="button" >Affichage global</button></a> &nbsp';
            $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'?aff=sequentiel&nb=0"><button class="btn btn-success"  role="button" >Affichage séquentiel</button></a></div>';
        }else{

            if($_GET['aff']=='global'){
                if(isset($this->box)) {
                    foreach ($this->box->contient()->get() as $q) {

                        $prest = Prestation::where('id',$q->idPrest)->first();

                        $r.='<div class="media" style="margin-left: 10%"><div class="media-left" >'
                            . '<a href="#">'
                            . '<img class="media-object" src="'.$route.'/../web/img/'.$prest->img.'" alt="'.$prest->nom.'" style="width: 128px; height: 128px;">'
                            . '</a>'
                            . '</div>'
                            . '<div class="media-body">'
                            . '<h4 class="media-heading">'.$prest->nom.'</h4>'
                            .'<p>'.$prest->categorie->nom.'<br>'.'</p>'
                            .'<p>'.$prest->descr.'</p>'
                            .'</div></div>'   ;
                    }

                    $r.='</div>';

                }
            }elseif($_GET['aff']=='sequentiel'){

                if(isset($_GET['nb'])){
                    $nb=$_GET['nb'];

                    $prest = Prestation::where('id',$this->box->contient()->get()[$nb]->idPrest)->first();

                    $r.='<div class="media" style="margin-left: 10%"><div class="media-left" >'
                        . '<a href="#">'
                        . '<img class="media-object" src="'.$route.'/../web/img/'.$prest->img.'" alt="'.$prest->nom.'" style="width: 128px; height: 128px;">'
                        . '</a>'
                        . '</div>'
                        . '<div class="media-body">'
                        . '<h4 class="media-heading">'.$prest->nom.'</h4>'
                        .'<p>'.$prest->categorie->nom.'<br>'.'</p>'
                        .'<p>'.$prest->descr.'</p>'
                        .'</div></div></div>';

                    $r.='<div class="container text-center">';

                    if($nb==0){
                        $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'?aff=sequentiel&nb='.($nb+1).'"><button class="btn btn-success"  role="button" style="width: 15%">Suivant</button></a>';
                    }elseif ($nb>0 && $nb<($this->box->nbArticles-1)){
                        $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'?aff=sequentiel&nb='.($nb-1).'"><button class="btn btn-success"  role="button" style="width: 15%">Precedent</button></a>&nbsp&nbsp';
                        $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'?aff=sequentiel&nb='.($nb+1).'"><button class="btn btn-success"  role="button" style="width: 15%">Suivant</button></a>';
                    }else{
                        $r.='<a href="'.$route.'/gift/'.$this->box->urlCadeau.'?aff=sequentiel&nb='.($nb-1).'"><button class="btn btn-success"  role="button" style="width: 15%">Precedent</button></a>';
                    }

                    $r.='</div>';

                }
            }
        }
        return $r;
    }

    public function htmlViewCagnotte($gest=false)
    {
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $host = \Slim\Slim::getInstance()->request()->getHost();

        $r = '<div class="container">';
        if (isset($_GET['error'])) {
            if ($_GET['error'] == 1) {
                $r .= '<div class="alert alert-danger" role="alert">Le montant saisi est incorrect.</div>';
            }
        }
        $r.='<div class="row"><div class="col-md-6">';
        $r.='<h1>Visualisation du coffret</h1>';
        if(isset($this->box)) {
            $cagnotte = Cagnotte::where('idbox', '=', $this->box->id)->first();
            foreach ($this->box->contient()->get() as $q) {
                $prest = Prestation::where('id', $q->idPrest)->first();

                $r .= '<div class="media"><div class="media-left" >'
                    . '<a href="#">'
                    . '<img class="media-object" src="' . $route . '/../web/img/' . $prest->img . '" alt="' . $prest->nom . '" style="width: 128px; height: 128px;">'
                    . '</a>'
                    . '</div>'
                    . '<div class="media-body">'
                    . '<h4 class="media-heading">' . $prest->nom . '</h4>'
                    . '<p>' . $prest->categorie->nom . '<br>' . '</p>'
                    . '<p>' . $prest->prix . '€ ' . '</p>'

                    . '</div></div>';
            }
            $r .= '</div><div class="col-md-6">';
            $r .= '<ul class="list-group">';

            $r .= '<br><br><label for="mon_id">Message</label><li class="list-group-item">' . $this->box->message . '</li>';
            $r .= '<label for="mon_id">Montant total</label><li class="list-group-item">' . $this->box->prixTotal . ' €</li>';
            $r .= '<label for="mon_id">Montant déjà payé</label><li class="list-group-item">' . $cagnotte->montant . ' €</li>';
            $r .= '</ul>';
            if($cagnotte->montant<$this->box->prixTotal && $cagnotte->status=='ouvert') {
                if ($gest) {
                    $r .= '<form id="form1" method="POST" action="' . $route . '/cagnotte/edit/pay/' . $cagnotte->tokenEdition . '">';
                } else {
                    $r .= '<form id="form1" method="POST" action="' . $route . '/cagnotte/pay/' . $cagnotte->tokenPartage . '">';
                }
                $pmax=$this->box->prixTotal - $cagnotte->montant;
                $r .= '<label for="pays">Entrez un montant ( maximum '. $pmax  . '€ ) :</label><br>';
                $r .= '<input type="text" name="montant" placeholder="montant"><br>';
                $r .= '<br><button type="submit" class="btn btn-primary">Payer</button>';
                $r .= '</form>';
            }else if($cagnotte->status=='ouvert'){
                if ($gest ) {
                    $r .= '<form id="form1" method="POST" action="' . $route . '/cagnotte/cloturer/' . $cagnotte->tokenEdition . '">';
                    $r .= '<br><button type="submit" class="btn btn-primary">Cloturer Cagnotte</button>';
                    $r .= '</form>';
                }
            }


        }
        $r.='</div></div>';

        return $r;
    }

    public function htmlViewCagnottePay(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r ='<div class="alert alert-info"><strong>Merci d\'avoir cotisé à la cagnotte.</strong></div>';
        $r .='<form style="display:inline" method="GET" action="' . $route . '/'.'">
                        <button type="submit" class="btn btn-primary">Retour à l\'acceuil</button>
                    </form>';
        return $r;
    }

    public function htmlViewCagnotteEditPay(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r ='<div class="alert alert-info"><strong>Merci d\'avoir cotisé à la cagnotte.</strong></div>';
        $r .='<form style="display:inline" method="GET" action="' . $route . '/box/view/'.$this->box->token.'">
                        <button type="submit" class="btn btn-primary">Retour à la box</button>
                    </form>';
        return $r;
    }

    public function render($i){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        switch($i){
            case '1':
                $content=$this->htmlViewBox();
                break;
            case '2':
                $content=$this->htmlValidateBox();
                break;
            case '6':
                $content=$this->htmlValidateRecap();
                break;
            case '8':
                $content=$this->htmlValidateDone();
                break;
            case '9':
                $content=$this->htmlViewBoxToken();
                break;
            case '10':
                $content=$this->htmlViewBoxPay();
                break;
            case '11':
                $content=$this->htmlViewBoxEdit();
                break;
            case '12':
                $content=$this->htmlViewLogIn();
                break;
            case '13':
                $content=$this->htmlViewGift();
                break;
            case '14':
                $content=$this->htmlViewCagnotte();
                break;
            case '15':
                $content=$this->htmlViewCagnottePay();
                break;
            case '16':
                $content=$this->htmlViewCagnotte(true);
                break;
            case '17':
                $content=$this->htmlViewCagnotteEditPay();
                break;
            default :
                $content='';
        }
        include 'header.php';
        echo <<<END

<!DOCTYPE html>
<html lang="fr">
     <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
       
        <link href="$route/../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="$route/../web/css/GiftBoxCss.css" rel="stylesheet" type="text/css">
        <title>Giftbox</title>        
    </head>
    
    <body>
        $header
        $content
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="$route/../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>

END;
    }
}