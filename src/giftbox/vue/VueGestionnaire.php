<?php
/**
 * Created by PhpStorm.
 * User: Pierre
 * Date: 04/01/2017
 * Time: 12:49
 */

namespace giftbox\vue;


class VueGestionnaire
{

    private $collection;

    function __construct($p)
    {
        $this->collection=$p;
    }

    public function htmlViewLogin()
    {
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r= '<div class="container">';
        if(isset($_GET['error'])){
            if($_GET['error']==1){
                $r.='<div class="alert alert-warning" role="alert">Veuillez verifier les données.</div>';
            }
            if($_GET['error']==2){
                $r.='<div class="alert alert-warning" role="alert">Le mot de passe ne correspond pas !</div>';
            }
            if($_GET['error']==3){
                $r.='<div class="alert alert-warning" role="alert">L\'adresse email est incorrect</div>';
            }
        }

        $r.='<div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
      <form class="form-signin" method="POST">
        <h2 class="form-signin-heading">Connexion</h2>
        <label for="inputEmail" class="sr-only">Adresse mail</label>
        <input type="text" id="inputEmail" class="form-control" name="connectMail" placeholder="Adresse mail" required autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" name="connectPass" placeholder="Mot de passe" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Se souvenir de moi
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
        </form>
        <br>
        
       
        <!-- Button trigger modal -->
<button type="button" class="btn btn-lg btn-warning btn-block" data-toggle="modal" data-target="#myModal">
  Afficher les gestionnaires
</button>
      </div>
        
    </div><!-- /row -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Liste des comptes gestionnaire</h4>
      </div>
      <div class="modal-body">
        <p>email : compte1@giftbox.com --- mdp : pass1</p>
         <p>email : compte2@giftbox.com --- mdp : pass2</p>
          <p>email : compte3@giftbox.com --- mdp : pass3</p>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
    </div> <!-- /container -->';

        return $r;
    }

    public function htmlViewAccueil(){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $r='
        <div class="container">
        <div class="text-center">';
        if(isset($_GET['modif'])) {
            if ($_GET['modif'] == 1) {
                $r .= '<div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   La prestation a été supprimée.
                </div>';
            }
            if ($_GET['modif'] == 2) {
                $r .= '<div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   La prestation a été desactivée.
                </div>';
            }
            if ($_GET['modif'] == 3) {
                $r .= '<div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   La prestation a été activée.
                </div>';
            }
        }
        $r.='<a href="'.$route.'/gest/addPrest"><button type="button" class="btn btn-lg btn-success">Ajouter une prestation</button></a>
        </div>
        <br>
        <div class="table-responsive">
        <table class="table table-hover" style="background-color: white">
        <thead>
              <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Prix</th>
                <th>Catégorie</th>
                <th>Etat</th>
                <th>Modifier</th>
                <th>Supprimer</th>
              </tr>
            </thead>
            <tbody>';
        foreach ($this->collection as $q){
            if($q->etat!='delete'){
                $r.='
              <tr>
                <td>'.$q->id.'</td>
                <td>'.$q->nom.'</td>
                <td>'.$q->prix.'€</td>
                <td>'.$q->categorie->nom.'</td>';
                if($q->etat=='actif'){
                    $r.='<td><span class="label label-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span></td>';
                    $r.=' <form id="formGestDisPrest" method="POST" action="' . $route . '/gest/disable/'.$q->id.'">';
                    $r.='<td><button type="submit" class="btn btn-info">Desactiver</button></td>
                    </form>';
                }else{
                    $r.='<td><span class="label label-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span></td>';
                    $r.=' <form id="formGestActiPrest" method="POST" action="' . $route . '/gest/active/'.$q->id.'">';
                    $r.='<td><button type="submit" class="btn btn-success">Activer</button></td>
                    </form>';
                }
                $r.=' <form id="formGestDelPrest" method="POST" action="' . $route . '/gest/delete/'.$q->id.'">';

                $r.='<td><button type="submit" class="btn btn-danger">Supprimer</button></td>
                    </form>';
                $r.='</tr>';
            }
        }
        $r.='
              
            </tbody>
    </table></div></div>';
        return $r;
    }

    public function htmlViewAddprest()
    {
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        $tab = explode('/', $route);
        if (isset($tab[1])){
            $chemin = $tab[1];
        }

        $r ='<div class="container">';
        if(isset($_GET['error'])) {
            if ($_GET['error'] == 1) {
                $r .= '<div class="alert alert-danger" role="alert">Veuillez vérifier les données.</div>';
            }
            if ($_GET['error'] == 2) {
                $r .= '<div class="alert alert-danger" role="alert">Erreur lors du transfert de l\'image.</div>';
            }
            if ($_GET['error'] == 3) {
                $r .= '<div class="alert alert-danger" role="alert">L\'image est au mauvais format.</div>';
            }
            if ($_GET['error'] == 4) {
                $r .= '<div class="alert alert-danger" role="alert">Erreur lors de l\'ecriture de l\'image sur le serveur.</div>';
            }
        }
        $r .='<div class="panel panel-primary">
                <div class="panel-heading">
                <h3 class="panel-title">Ajout d\'une prestation</h3>
              </div> <div class="panel-body">';

        $r.='<form id="form1" method="POST" action="' . $route . '/gest/addPrest" enctype="multipart/form-data">';
        $r.='<div class="form-group row">
                <label class="col-sm-2 col-form-label" >Titre de la prestation</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="titrePrest" placeholder="Titre" required>
              </div>        
        </div>';
        $r.='<div class="form-group row">
                <label class="col-sm-2 col-form-label" >Description</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="description" placeholder="Description" required>
              </div>        
        </div>';
        $r.='<div class="form-group row">
                <label class="col-sm-2 col-form-label" >Prix</label>
              <div class="col-sm-10">
                   <div class="input-group">
                <input type="text" class="form-control" name="prix" placeholder="Prix" required><div class="input-group-addon">€</div>
                </div>
              </div>
                      
        </div>';
        $r.='<div class="form-group row">
            
              <label class="col-sm-2 col-form-label">Categorie</label>
              <div class="col-sm-10">
                  <select name="categorie" class="form-control">';
        foreach ($this->collection as $c){
            $r.='<option>'.$c->nom.'</option>';
        }

        $r.='</select>
                
        
        </div></div>';
        $r.='<div class="form-group row">
            
              <label class="col-sm-2 col-form-label">Image (jpg, jpeg, gif, png)</label>
              <div class="col-sm-10">
              <input name="photo" type="file" id="exampleInputFile" accept="image/*">
                
        
        </div></div>
        <fieldset class="form-group row">
      <legend class="col-form col-sm-2">Etat</legend>
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="etat" id="gridRadios1" value="actif" checked>
            Active
          </label>
        </div>
        <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="etat" id="gridRadios2" value="dis">
           Desactive
          </label>
        </div>
        
    </fieldset>
        ';

        $r .= '<div class="text-center"> <button type="submit" class="btn btn-lg btn-success">Ajouter la prestation</button></div>';
        $r .='</form></div></div></div>';
        return $r;
    }

    public function render($i){
        $route = \Slim\Slim::getInstance()->request()->getRootUri();
        switch($i){
            case '1':
                $content=$this->htmlViewLogin();
                break;
            case '2':
                $content=$this->htmlViewAccueil();
                break;
            case '3':
                $content=$this->htmlViewAddPrest();
                break;
            default :
                $content='';
        }
        include 'header.php';
        echo <<<END

<!DOCTYPE html>
<html lang="fr">
     <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
       
        <link href="$route/../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="$route/../web/css/GiftBoxCss.css" rel="stylesheet" type="text/css">
        <title>Giftbox</title>        
    </head>
    
    <body>
        $header
        $content
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="$route/../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>

END;
    }
}
