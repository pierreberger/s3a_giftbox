<?php
/**
 * Created by PhpStorm.
 * User: Pierre
 * Date: 14/12/2016
 * Time: 14:49
 */

namespace giftbox\controllers;

use giftbox\models\Cagnotte;
use \giftbox\models\Categorie;
use giftbox\models\Contient;
use \giftbox\models\Prestation;
use giftbox\vue\VueBox;

use giftbox\models\Box;
use giftbox\vue\VueCatalogue;

use \RandomLib\Factory;
use \SecurityLib;

use \PasswordPolicy\Policy;


class ControlBox
{

    public function viderPanier(){
        unset($_SESSION['box']);
        self::creation();
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->request->getRootUri().'/box/view');
    }


    public function ajouterPrestation($idPrestation){

        self::creation();
        $p = Prestation::where('id',$idPrestation)->first();
        if(!is_null($p)){
            $_SESSION['box'][] = $p;
            $_SESSION['prixTotal'] += $p->prix;
            $_SESSION['nbArticle'] += 1;

        }
    }


    public function afficherBox(){
        self::creation();
        $v=new VueBox();
        $v->render(1);
    }

    public function supprimerPanier($id){
        foreach ($_SESSION['box'] as $i=>$p){
            if($p->id == $id){
                $_SESSION['nbArticle']--;
                $_SESSION['prixTotal'] -= $p->prix;
                unset($_SESSION['box'][$i]);
                break;
            }
        }
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->request->getRootUri().'/box/view');
    }

    public function supprimerArticle($token,$id){
        $b = Box::where('token',$token)->first();

        if($b!=null){
            $p = Prestation::where('id',$id)->first();

            $b->nbArticles++;
            $b->prixTotal += $p->prix;

            $c = new Contient();
            $c->idBox = $b->id;
            $c->idPrest = $p->id;

            $b->save();
            $c->save();
        }
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->request->getRootUri().'/box/edit/'.$b->token);
    }

    public function afficherBoxToken($token){

        $token = filter_var($token, FILTER_SANITIZE_STRING);
        $q = Box::where('token',$token)->first();
        if($q!=null ){
            if(is_null($q->pass)) {
                $_SESSION['authBox'] = $token;
            }

            if(isset($_SESSION['authBox']) && $_SESSION['authBox']==$token){
                $v=new VueBox();
                $v->setBox($q);
                $v->render(9);
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/login/'.$token);
            }

        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri().'/box/view?error=3');
        }


    }

    public function validerPanier(){
        if(isset($_SESSION['box'])){
            $v=new VueBox();
            if(empty($_SESSION['box'])){

                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/view?error=1');
            }elseif(self::estvalide($_SESSION['box'])){
                $v->render(2);
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/view?error=2');
            }

        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri());
        }
    }

    public function validateRecap(){

        if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['email']) && isset($_POST['message'])
            && isset($_POST['modepay'])){
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $modepay = $_POST['modepay'];

            if(!filter_var($nom, FILTER_SANITIZE_STRING) || !filter_var($email, FILTER_VALIDATE_EMAIL)
                || !filter_var($prenom, FILTER_SANITIZE_STRING) || !filter_var($message, FILTER_SANITIZE_STRING)
                || !filter_var($modepay, FILTER_SANITIZE_STRING)){
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/validateInfo?error=1');
            }else{

                $_SESSION['prenom'] = filter_var($prenom, FILTER_SANITIZE_STRING);
                $_SESSION['nom'] = filter_var($nom, FILTER_SANITIZE_STRING);
                $_SESSION['email'] = filter_var($email, FILTER_SANITIZE_EMAIL);
                $_SESSION['message']= filter_var($message, FILTER_SANITIZE_STRING);
                $_SESSION['modepay']= filter_var($modepay, FILTER_SANITIZE_STRING);

                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/recap');
            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri().'/box/validateInfo?error=10');
        }
    }

    public function recap(){
        $v=new VueBox();
        $v->render(6);
    }

    public function validate(){

        if(isset($_SESSION['box'])) {
            $factory = new Factory;
            $generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
            $generator = $factory->getMediumStrengthGenerator();
            $token = $generator->generateString(20, 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $_SESSION['token']= $token;
            $b = new Box();
            $b->nom = $_SESSION['nom'];
            $b->prenom = $_SESSION['prenom'];
            $b->mail = $_SESSION['email'];
            $b->message = $_SESSION['message'];
            $b->token = $_SESSION['token'];
            $b->prixtotal = $_SESSION['prixTotal'];
            $b->nbarticles = $_SESSION['nbArticle'];
            $b->etat='valider';
            $b->modepay = $_SESSION['modepay'];


            if(strlen($_POST['pass']) != 0){
                $policy = new Policy;
                $policy->contains('alnum', $policy->atLeast(4));
                $r = $policy->test($_POST['pass']);
                if(!$r->result){
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri().'/box/recap?error=3');
                }
                if ($_POST['pass'] == $_POST['passConfirm']) {
                    $b->pass = password_hash($_POST['pass'], PASSWORD_DEFAULT, array("cost" => 12));
                } else {
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri().'/box/recap?error=2');
                }
            }

            //var_dump($_SESSION['box']);
            $b->save();

            if($_SESSION['modepay']=='cagnotte'){
                $tokenEdition = $generator->generateString(20, 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                $tokenPartage = $generator->generateString(20, 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                $cagnotte=new Cagnotte();
                $cagnotte->idBox=$b->id;
                $cagnotte->montant=0.;
                $cagnotte->tokenPartage=$tokenPartage;
                $cagnotte->tokenEdition=$tokenEdition;
                $cagnotte->status='ouvert';
                $cagnotte->save();
            }

            foreach ($_SESSION['box'] as $p) {
                $c = new Contient();
                $c->idbox = $b->id;
                $c->idprest = $p->id;
                $c->save();
            }
            unset($_SESSION['box']);
            unset($_SESSION['pass']);
            unset($_SESSION['nbArticle']);

            $_SESSION['prixTotal']=0;


            $v = new VueBox();
            $v->render(8);
        }else{

            $v = new VueBox();
            $v->render(8);
        }
    }

    public function pay($b){

        $q = Box::where('token',$b)->first();

        if(isset($q)){

            if (isset($_SESSION['authBox']) && $_SESSION['authBox'] == $b && $q->modePay=='classique') {
                $q->etat = 'paye';
                $q->save();
                $v = new VueBox();
                $v->setBox($q);
                $v->render(10);
            } else {
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri() . '/box/login/' . $b);
            }

        }else{
            $v = new VueBox();
            $v->render(1);
        }
    }

    public function editBox($b){

        $q = Box::where('token',$b)->first();
        if(isset($q)){

            if(isset($_SESSION['authBox']) && $_SESSION['authBox']==$b){
                $v=new VueBox();
                $v->setBox($q);
                $v->render(11);
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/login/'.$b);
            }

        }else{
            $v = new VueBox();
            $v->render(1);
        }

    }

    public function delete($box,$id){

        $b = Box::where('token',$box)->first();

        if(isset($b)){
            $q = Contient::where('idBox',$b->id)->where('idPrest',$id)->first();
            $p = Prestation::where('id', $id)->first();


            $retour = new Contient();
            $retour->idBox=$b->id;
            $retour->idPrest=$id;

            $q->delete();


            $co= array();
            foreach ($b->contient()->get() as $q) {

                $prest = Prestation::where('id', $q->idPrest)->first();
                $co[] = $prest;
            }

            if(!self::estvalide($co)){
                $retour->save();
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/edit/'.$b->token.'?error=1');
            }else{
                $b->prixTotal -= $p->prix;
                $b->nbArticles -= 1;
                $b->save();
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/edit/'.$b->token);
            }

        }

    }

    private static function estvalide($co){
        $deuxCat=false;
        $cat= $co[0]->categorie;
        foreach ($co as $p){
            $cat1=$p->categorie;
            if($cat!=$cat1)
                $deuxCat=true;
        }
        if($deuxCat) {
            return true;
        }else{
            return false;
        }
    }

    public static function creation(){
        if(!isset($_SESSION['box'])) {
            $_SESSION['box'] = array();
            $_SESSION['nbArticle'] = 0;
            $_SESSION['prixTotal'] = 0;
        }
    }

    public function login($b){

        $box = Box::where('token',$b)->first();
        if(!is_null($box)){
            if(is_null($box->pass)){
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/box/view/'.$b);
            }else{

                if(isset($_SESSION['authBox'])) {
                    if ($_SESSION['authBox'] == $b) {
                        $app = \Slim\Slim::getInstance();
                        $app->redirect($app->request->getRootUri() . '/box/view/' . $b);
                    }
                }
                $v = new VueBox();
                $v->setBox($box);
                $v->render(12);
            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri() . '/box/view?error=3');
        }

    }

    public function auth($b){

        $box = Box::where('token',$b)->first();

        if(password_verify($_POST['pass'], $box->pass)){
            $_SESSION['authBox']=$b;
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri().'/box/view/'.$b);
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri().'/box/login/'.$b.'?error=1');
        }


    }

    public function genererCadeau($box){
        $box = Box::where('token',$box)->first();

        $factory = new Factory;
        $generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
        $generator = $factory->getMediumStrengthGenerator();
        $token = $generator->generateString(20, 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        $box->urlCadeau=$token;
        $box->save();



    }

    public function giftView($id){
        $box = Box::where('urlCadeau',$id)->first();

        $box->etat='Visitée par le destinataire';
        $box->save();

        $v = new VueBox();
        $v->setBox($box);
        $v->render(13);

    }

    public function cagnotteView($id){
        $cagnotte = Cagnotte::where('tokenPartage', $id)->first();

        if(isset($cagnotte)) {
            $box=Box::where('id','=',$cagnotte->idBox)->first();
            if(isset($box)) {
                $v = new VueBox();
                $v->setBox($box);
                $v->render(14);
            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri() . '/');
        }
    }

    public function cagnotteViewEdit($id){

        $cagnotte = Cagnotte::where('tokenEdition', $id)->first();

        if(isset($cagnotte)) {
            $box=Box::where('id','=',$cagnotte->idBox)->first();
            if(isset($box)) {
                if(isset($_SESSION['authBox']) && $_SESSION['authBox']==$box->token){
                    $v = new VueBox();
                    $v->setBox($box);
                    $v->render(16);
                }else{
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri().'/box/login/'.$box->token);
                }

            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri() . '/?error=1');
        }


    }

    public function cagnottePay($c,$gest=false)
    {

        if ($gest) {
            $q = Cagnotte::where('tokenEdition', $c)->first();
        } else {
            $q = Cagnotte::where('tokenPartage', $c)->first();
        }


        if (isset($q)) {
            $box = Box::where('id', '=', $q->idBox)->first();
            if (isset($box)) {

                if (isset($_POST['montant']) && filter_var($_POST['montant'], FILTER_VALIDATE_FLOAT) && $q->status=='ouvert') {

                    $m = $_POST['montant'];
                    if ($m > 0 && $m <= $box->prixTotal - $q->montant) {
                        $q->montant += $m;
                        $q->save();
                        $v = new VueBox();
                        $v->setBox($box);
                        if ($gest) {
                            $v->render(17);
                        } else {
                            $v->render(15);
                        }
                    } else {
                        $app = \Slim\Slim::getInstance();
                        if ($gest) {
                            $app->redirect($app->request->getRootUri() . '/cagnotte/edit/' . $c . '?error=1');

                        } else {
                            $app->redirect($app->request->getRootUri() . '/cagnotte/' . $c . '?error=1');
                        }
                    }

                } else {
                    $app = \Slim\Slim::getInstance();
                    if ($gest) {
                        $app->redirect($app->request->getRootUri() . '/cagnotte/edit/' . $c . '?error=1');

                    } else {
                        $app->redirect($app->request->getRootUri() . '/cagnotte/' . $c . '?error=1');
                    }
                }
            }

        } else {
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri() . '/?error=1');
        }
    }

    public function cloturerCagnotte($c){
        $q = Cagnotte::where('tokenEdition', $c)->first();
        if (isset($q)) {
            $box = Box::where('id', '=', $q->idBox)->first();
            if (isset($box)) {
                if (isset($_SESSION['authBox']) && $_SESSION['authBox'] == $box->token && $q->montant>=$box->prixTotal) {
                    $q->status='ferme';
                    $q->save();
                    $box->etat = 'paye';
                    $box->save();
                    $v = new VueBox();
                    $v->setBox($box);
                    $v->render(10);
                } else {
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri() . '/box/login/' . $box->token);
                }
            }
        }else {
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri() . '/?error=1');
        }
    }

}