<?php
/**
 * Created by PhpStorm.
 * User: Pierre
 * Date: 04/01/2017
 * Time: 12:51
 */

namespace giftbox\controllers;


use giftbox\models\Categorie;
use giftbox\models\Gestionnaire;
use giftbox\models\Prestation;
use giftbox\vue\VueGestionnaire;
use \PasswordPolicy\Policy;

class ControlGestionnaire
{

    public function login(){
        if(isset($_SESSION['gestionnaire'])) {
            if ($_SESSION['gestionnaire'] == 1) {
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri() . '/gest/auth');
            }else{
                $v=new VueGestionnaire('');
                $v->render(1);
            }
        }else{
            $v=new VueGestionnaire('');
            $v->render(1);
        }

    }

    public function loginPost(){
        if (isset($_POST['connectMail']) && isset($_POST['connectPass'])){
            $mail = $_POST['connectMail'];
            $pass = $_POST['connectPass'];
            if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri().'/gest/login?error=1');
            }else{
                $gest = Gestionnaire::where('mail',$mail)->first();
                if(!is_null($gest)) {
                    if (password_verify($pass, $gest->password)) {
                        $_SESSION['gestionnaire']=1;
                        $app = \Slim\Slim::getInstance();
                        $app->redirect($app->request->getRootUri() . '/gest/auth');
                    } else {
                        //Mdp incorrect
                        $app = \Slim\Slim::getInstance();
                        $app->redirect($app->request->getRootUri() . '/gest/login?error=2');
                    }
                }else{
                    //Email incorrect
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri() . '/gest/login?error=3');
                }
            }
        }else{
            echo 'o';
            echo $_POST['connectPass'];
        }
    }

    public function accueil(){
        if(isset($_SESSION['gestionnaire'])){
            if($_SESSION['gestionnaire']==1){
                $q = Prestation::all();
                $v=new VueGestionnaire($q);
                $v->render(2);
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri());
            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri());
        }

    }

    public function delete($id){
        $p = Prestation::where('id',$id)->first();
        if(isset($p)){
            $p->etat='delete';
            $p->save();
        }

    }

    public function disable($id){
        if(isset($_SESSION['gestionnaire'])) {
            if ($_SESSION['gestionnaire'] == 1) {
                $p = Prestation::where('id',$id)->first();
                if(isset($p)){
                    $p->etat='dis';
                    $p->save();
                }
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri());
            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri());
        }
    }

    public function active($id){
        if(isset($_SESSION['gestionnaire'])) {
            if ($_SESSION['gestionnaire'] == 1) {
                $p = Prestation::where('id',$id)->first();
                if(isset($p)){
                    $p->etat='actif';
                    $p->save();
                }else{
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri());
                }
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri());
            }
        }
    }

    public function addPrest(){
        if(isset($_SESSION['gestionnaire'])) {
            if ($_SESSION['gestionnaire'] == 1) {

                $c = Categorie::all();
                $v = new VueGestionnaire($c);
                $v->render(3);
            }else{
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->request->getRootUri());
            }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri());
        }
    }

    public function addPrestPost(){
        if(isset($_SESSION['gestionnaire'])) {
            if ($_SESSION['gestionnaire'] == 1) {
                if(isset($_POST['titrePrest']) && isset($_POST['description']) && isset($_POST['prix']) &&
                    isset($_POST['categorie'])&& isset($_POST['etat']) && isset($_FILES['photo'])){
                    if (!$_FILES['photo']['error'] > 0){

                        $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png');
                        $extension_upload = strtolower(  substr(  strrchr($_FILES['photo']['name'], '.')  ,1)  );
                        if (in_array($extension_upload,$extensions_valides) ) {
                            $titre = $_POST['titrePrest'];
                            $descr = $_POST['description'];
                            $prix = $_POST['prix'];
                            $categorie = $_POST['categorie'];
                            $etat = $_POST['etat'];
                            $nomimage = $_FILES['photo']['name'];
                            if(!filter_var($titre, FILTER_SANITIZE_STRING) || !filter_var($descr, FILTER_SANITIZE_STRING)
                                || !filter_var($prix, FILTER_VALIDATE_FLOAT) || !filter_var($categorie, FILTER_SANITIZE_STRING)
                                || !filter_var($etat, FILTER_SANITIZE_STRING) ||!filter_var($nomimage, FILTER_SANITIZE_STRING)){
                                $app = \Slim\Slim::getInstance();
                                $app->redirect($app->request->getRootUri().'/gest/addPrest?error=1');
                            }else {
                                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                                $descr = filter_var($descr, FILTER_SANITIZE_STRING);
                                //$prix = filter_var($prix, FILTER_SANITIZE_NUMBER_FLOAT);
                                $categorie = filter_var($categorie, FILTER_SANITIZE_STRING);
                                $nomimage = filter_var($nomimage, FILTER_SANITIZE_STRING);

                                $resultat = move_uploaded_file($_FILES['photo']['tmp_name'],'web/img/'.$nomimage);
                                if($resultat){
                                    $p = new Prestation();
                                    $p->nom = $titre;
                                    $p->descr = $descr;
                                    $p->prix = $prix;
                                    $p->etat = $etat;
                                    $p->img = $nomimage;
                                    $c = Categorie::where('nom', $categorie)->first();
                                    if (isset($c)) {
                                        $p->cat_id = $c->id;
                                    }
                                    $p->save();
                                    $app = \Slim\Slim::getInstance();
                                    $app->redirect($app->request->getRootUri().'/gest/auth');
                                }else{
                                    $app = \Slim\Slim::getInstance();
                                    $app->redirect($app->request->getRootUri().'/gest/addPrest?error=4');
                                }

                            }
                        }else{
                            $app = \Slim\Slim::getInstance();
                            $app->redirect($app->request->getRootUri().'/gest/addPrest?error=3');
                        }
                    }else{
                        $app = \Slim\Slim::getInstance();
                        $app->redirect($app->request->getRootUri().'/gest/addPrest?error=2');
                    }
                }else{
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->request->getRootUri().'/gest/addPrest?error=1');

                }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri());
        }
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri());
        }

    }
}