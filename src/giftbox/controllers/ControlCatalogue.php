<?php

namespace giftbox\controllers;

use \giftbox\models\Categorie;
use giftbox\models\Notation;
use \giftbox\models\Prestation;
use \giftbox\vue\VueCatalogue;

class ControlCatalogue
{

    public function listerPrestations(){
        $q = Prestation::all();
        $p = new VueCatalogue($q);
        $p->render(1);
    }

    public function viewPrestation($id){
        $q = Prestation::where('id',$id)->first();
        if(!is_null($q) && $q->etat=='actif') {
            $p = new VueCatalogue($q);
            $p->render(2);
        }else{
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri().'/prestations/listPrest?error=1');
        }
    }

    public function listeCategorie(){
        $cat = Categorie::get();

            $p = new VueCatalogue($cat);
            $p->render(3);


    }

    public function viewPrestationCat($idCategorie){
        $q = Prestation::where('cat_id', $idCategorie)->where('etat','actif')->get();
        $c = Categorie::where('id', $idCategorie)->first();

        if(!is_null($c)){
            $p = new VueCatalogue($q);
            $p->render(1);
        }else{

            $app = \Slim\Slim::getInstance();
            $app->redirect($app->request->getRootUri().'/prestations/listCat?error=1');
        }
    }

    public function viewPrestationPrixAsc(){
        $q = Prestation::orderBy('prix')->get();
        $p = new VueCatalogue($q);
        $p->render(1);
    }

    public function viewPrestationPrixDesc(){
        $q = Prestation::orderBy('prix','DESC')->get();
        $p = new VueCatalogue($q);
        $p->render(1);
    }

    public function validateRate(){
        $p = new VueCatalogue(null);
        $p->render(4);
    }

    public function rate($id,$note){
        $n = new Notation();
        $n->idprestation = $id;
        $n->note = $note;
        $n->save();
    }

    public function acceuil(){
        $p= array();
        $p['top']=self::noteMax(Prestation::where('etat','actif')->get());
        $p['topCat']=array();
        $p['topCat'][]=self::noteMax(Prestation::where('cat_id', 1)->where('etat','actif')->get());
        $p['topCat'][]=self::noteMax(Prestation::where('cat_id', 2)->where('etat','actif')->get());
        $p['topCat'][]=self::noteMax(Prestation::where('cat_id', 3)->where('etat','actif')->get());
        $p['topCat'][]=self::noteMax(Prestation::where('cat_id', 4)->where('etat','actif')->get());
        $v = new VueCatalogue($p);
        $v->render(5);
    }

    private static function noteMax($co){
        $p = $co[0];
        $max = -1;
        foreach ($co as $q){
            $note=$q->note()->avg('note');
            if($note>=$max){
                $p=$q;
                $max=$note;
            }

        }
        return $p;
    }

}