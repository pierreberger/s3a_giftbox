<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 04/01/2017
 * Time: 15:06
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class Cagnotte extends Model
{
    protected $table = 'cagnotte';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function box(){
        return $this->belongsTo('giftbox\models\Box', 'idbox');
    }

}