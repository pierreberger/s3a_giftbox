<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 14/12/2016
 * Time: 15:11
 */

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model;

class Contient extends Model{

    protected $table = 'contient';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function box(){
        return $this->belongsTo('giftbox\models\Box', 'idbox');
    }

    public function prestation(){
        return $this->belongsTo('giftbox\models\Prestation', 'idprest');
    }

}