<?php

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function categorie(){
        return $this->belongsTo('giftbox\models\Categorie','cat_id');
    }

    public function contient(){
        return $this->hasMany('gifbox\models\Contient', 'idprest');
    }

    public function note(){
        return $this->hasMany('giftbox\models\Notation','idprestation');
    }
}