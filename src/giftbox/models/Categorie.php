<?php

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function prestations(){
        return $this->hasMany('giftbox\models\Prestation','cat_id');
    }
}