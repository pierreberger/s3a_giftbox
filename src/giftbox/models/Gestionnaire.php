<?php
/**
 * Created by PhpStorm.
 * User: Pierre
 * Date: 04/01/2017
 * Time: 17:49
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class Gestionnaire extends Model
{

    protected $table = 'gestionnaire';
    protected $primaryKey = 'mail';
    public $timestamps = false;

}