<?php
/**
 * Created by PhpStorm.
 * User: Axel
 * Date: 22/12/2016
 * Time: 00:32
 */

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model;

class Notation extends Model
{
    protected $table = 'notation';
    protected $primaryKey = 'id';
    public $timestamps = false;


}