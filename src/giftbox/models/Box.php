<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 14/12/2016
 * Time: 14:54
 */

namespace giftbox\models;

use \Illuminate\Database\Eloquent\Model;

class Box extends Model{

    protected $table = 'box';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function contient(){
        return $this->hasMany('giftbox\models\Contient','idbox');
    }




}